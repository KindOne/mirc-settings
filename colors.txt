[colors]
;               0                 1                   2                    3
;               1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5  6 7 8 9 0  1
n0=mIRC Classic,0,6,4,5,2,3,3,3,3,3,3,1,5,7,6,1,3,2,3,5,1,0,1,0,1,15,6,0,0,1,97

01 Appearance background
02 Action text
03 Ctcp text
04 Highlight text
05 Info text
06 Info2 text
07 Invite text
08 Join text
09 Kick text
10 Mode text
11 Nick text
12 Normal text
13 Notice text
14 Notify text
15 Other text
16 Own text
17 Part text
18 Quit text
19 Topic text
20 Wallops text
21 Whois text
22 Editbox text background
23 Editbox text
24 Listbox text background
25 Listbox text
26 Gray text
27 Title text
28 Inactive background
29 Treebar text background
30 Treebar text
31 MDI area